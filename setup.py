"""
to manually install the module in ./src run
pip3 install -e .
"""

from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    author='davus.g@gmail.com',
    license='proprietary',
)