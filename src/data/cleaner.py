import pandas as pd

class Cleaner:

    def __init__(self, verbose=True):
        self.verbose = verbose

    def clean(self, df, columns_to_clean):
        df = df.copy()                                 #to prevent changes to original data

        nof_rows_before_cleaning = df.shape[0]        #nof = numer of

        for columns in columns_to_clean:

            for name in columns.names:

                if not name in df.columns.values:       #honestly unnecessary as python would raise an exception itself but yeah
                    raise Exception(f"Key {name} not present in the data frame.")


                if(columns.drop_na):
                    df.dropna(how='any', inplace=True,subset=[name])

                if(columns.to_int):
                    df[name] = pd.to_numeric(df[name],errors='coerce').round(0).fillna(-1).astype(int)
        
        nof_rows_after_cleaning = df.shape[0]

        if self.verbose:
            print(f"Removed {nof_rows_after_cleaning - nof_rows_before_cleaning} rows during cleaning.")
            print(f"{nof_rows_after_cleaning} rows remaining.")


        return df


class ColumnsToClean:

    def __init__(self, names, drop_na=False, to_int=False):
        self.names = names
        self.drop_na = drop_na
        self.to_int = to_int

def test_nan():
    df = pd.DataFrame(data=[["Foo",1,47.11],["Bar",2,float('nan')],[float('nan'),"3",float('nan')]],columns=["string","int","decimals"])
    print(df)

    cleaner1 = Cleaner()
    df = cleaner1.clean(df, [ColumnsToClean(["string", "decimals"], drop_na=True, to_int=False), ColumnsToClean(["int"], drop_na=True, to_int=True)])   
    assert df.values.tolist() == [["Foo",1,47.11]]
    print("\n ----\n test_nan passed!\n ----")
    return

def test_to_int():
    df = pd.DataFrame(data=[["Foo",1,47.11],["Bar","männlich",float('nan')],[float('nan'),"3",float('nan')]],columns=["string","int","decimals"])
    print(df)

    cleaner_ints = Cleaner()
    df = cleaner_ints.clean(df,[ColumnsToClean(["int"], drop_na=False, to_int=True)])
    assert df["int"].values.tolist()==[1, -1, 3]
    print("\n ----\n test_to_int passed!\n ----")
    return

def test_for_non_existent_column():
    df = pd.DataFrame(data=[["Foo",1,47.11],["Bar","männlich",float('nan')],[float('nan'),"3",float('nan')]],columns=["string","int","decimals"])
    cleaner1 = Cleaner()

    try:
        cleaner1.clean(df, [ColumnsToClean(["non_existent column", "decimals"], drop_na=True, to_int=False), ColumnsToClean(["int"], drop_na=True, to_int=True)])   
    except Exception:
        print("\n ----\n test_iqr_for_non_existent_column passed!\n ----")
    else:
        raise Exception
    return

test_nan()

test_to_int()

test_for_non_existent_column()