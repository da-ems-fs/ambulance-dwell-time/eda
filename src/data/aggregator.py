import pandas as pd
import numpy as np

def mean_and_variance_of_df_by_group(df, groups_column, times_column):
    df_aggd=df[[times_column, groups_column]].groupby([groups_column]).agg(["mean", np.std, "count"])
    df_aggd.columns=df_aggd.columns.droplevel()
    df_mean_and_variance_by_group=df_aggd
    return df_mean_and_variance_by_group