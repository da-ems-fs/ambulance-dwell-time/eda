import pandas as pd

class DataConverter:
    
    def __init__(self, verbose=True):
        self.verbose = verbose
    
    def data_converter(self, df, columns_to_convert):
        df = df.copy()
        nof_rows_before_converting = df.shape[0]

        for columns in columns_to_convert:
        
            for name in columns.names:

                if(not name in df.columns.values):
                    raise Exception(f"Key {name} not present in data frame.")
                
                if(columns.convert_from_seconds_to_minutes):
                    df[name] = df[name].div(60)
                    if "second" in name.lower():
                        name_new = name
                        name_new = name_new.replace("second", "minute",1)  #,1 prevents it happening twice (honestly unnecessary)
                        name_new = name_new.replace("Second", "Minute",1)  # since "seconds" contains "second", it will also be changed to "Minute"+"s"
                        df = df.rename(columns={name: name_new})
                        name = name_new

                if(columns.convert_round_decimals):
                    df[name] = df[name].round(int(columns.convert_round_decimals))

                if(columns.pseudonymize):
                    unique_values = df[name].value_counts().index.to_list()
                    renames = dict(zip(unique_values, range(len(unique_values))))
                    df[name] = df[name].replace(renames)
                    if type(columns.pseudonymize)==str:
                        prefix = columns.pseudonymize
                        df[name] = prefix + df[name].astype(str)



        nof_rows_after_converting = df.shape[0]

        if(self.verbose):
            print(f"Removed {nof_rows_after_converting - nof_rows_before_converting} rows during cleaning")
            print(f"{nof_rows_after_converting} rows remaining.")
        return df

class ColumnsToConvert:
    def __init__(self, names, convert_from_seconds_to_minutes=False, convert_round_decimals=False, pseudonymize=False):
        self.names = names
        self.convert_from_seconds_to_minutes = convert_from_seconds_to_minutes
        self.convert_round_decimals = convert_round_decimals
        self.pseudonymize = pseudonymize

def test_convert_from_seconds_to_minutes():
    df = pd.DataFrame(data=[60,120,6,12],columns=["numeric_values"])
    print(df)
    converter1=DataConverter()
    df =converter1.data_converter(df,[ColumnsToConvert(["numeric_values"],convert_from_seconds_to_minutes=True)])
    print(df)
    print(df["numeric_values"].values.tolist())
    assert df["numeric_values"].values.tolist()==[1,2,0.1,0.2]
    print("\n ----\n test_convert_from_seconds_to_minutes passed!\n ----")

    return

def test_convert_round_decimals():
    df = pd.DataFrame(data=[[60.4444,120,11.323],[1.1,1.2,1.3]],columns=["numeric_values", "foo", "bar"])
    print(df)
    converter1=DataConverter()
    df =converter1.data_converter(df,[ColumnsToConvert(["numeric_values","bar"],convert_round_decimals=3)])
    print(df[["numeric_values", "bar"]].values.tolist())
    assert df["numeric_values"].values.tolist()==[60.444, 1.1]
    print("\n ----\n test_convert_round_decimals passed!\n ----")

    return

def test_convert_from_seconds_to_minutes_rename_column():
    df = pd.DataFrame(data=[60,120,6,12],columns=["numeric_values_seconds"])
    print(df)
    converter1=DataConverter()
    df =converter1.data_converter(df,[ColumnsToConvert(["numeric_values_seconds"],convert_from_seconds_to_minutes=True)])
    print(df.columns.values.tolist())
    assert df.columns.values.tolist()==['numeric_values_minutes']
    print("\n ----\n test_convert_from_seconds_to_minutes_rename_column passed!\n ----")

    return

def test_convert_from_seconds_to_minutes_rename_column_with_spaces():
    df = "nix"
    df = pd.DataFrame(data=[60,120,6,12],columns=["numeric_blablub_seconds with spaces"])
    print(df)
    print(df.columns.values)

    converter1=DataConverter()
    df =converter1.data_converter(df,[ColumnsToConvert(["numeric_blablub_seconds with spaces"],convert_from_seconds_to_minutes=True, convert_round_decimals=2)])
    print(df.columns.values)
    #assert df.columns.values.tolist()==['numeric_blablub_seconds with spaces']
    print("\n ----\n test_convert_from_seconds_to_minutes_rename_column_with_spaces passed!\n ----")

test_convert_from_seconds_to_minutes()

test_convert_round_decimals()

test_convert_from_seconds_to_minutes_rename_column()

test_convert_from_seconds_to_minutes_rename_column_with_spaces()