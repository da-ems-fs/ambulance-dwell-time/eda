from src.data.importer import Importer
from src.data.cleaner import Cleaner,ColumnsToClean
from src.data.datafilter import DataFilter,ColumnsToFilter
from src.data.dataconverter import DataConverter, ColumnsToConvert
import pandas as pd


class DataPreparer:
    def __init__(self):
        self.importer1 = Importer(column_separator = '\t')
        self.cleaner1 = Cleaner(verbose=True)
        self.filter1=DataFilter()
        self.converter1=DataConverter()


    def load_standard_for_eda(self, filename="data/export1.csv", top10=False):
        column_names = ["Code","Urgent","Year","Month","DayOfWeek","HourOfDay","3 - SecondsToMissionSite","4 - SecondsAtMissionSite","7 - SecondsToDestination","8 - SecondsAtDestination","DestinationName"]
        column_names_times = ["3 - SecondsToMissionSite","4 - SecondsAtMissionSite","7 - SecondsToDestination","8 - SecondsAtDestination"]
        df = self.importer1.import_file(filename, column_names)
        df = self.cleaner1.clean(df,[ColumnsToClean(names=["Urgent"],to_int=True)])
        df = self.cleaner1.clean(df,[ColumnsToClean(names=column_names,drop_na=True)])
        df=self.filter1.data_filter(df,[ColumnsToFilter(["Urgent"],filter_is_exactly=1)])
        df=self.filter1.data_filter(df,[ColumnsToFilter(column_names_times,remove_outliers_hard_limit=60*60*5)])
        if top10:
            df=self.filter1.data_filter(df, [ColumnsToFilter(["Code", "DestinationName"], filter_top_n=10)])
        df_minutes = self.converter1.data_converter(df, [ColumnsToConvert(column_names_times, convert_from_seconds_to_minutes=True, convert_round_decimals=2)])
        column_names_minutes_times = ["3 - MinutesToMissionSite","4 - MinutesAtMissionSite","7 - MinutesToDestination","8 - MinutesAtDestination"]
        return df_minutes, column_names_minutes_times