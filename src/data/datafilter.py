import pandas as pd

class DataFilter:

    def __init__(self, verbose=True):
        self.verbose = verbose

    def data_filter(self, df, columns_to_filter):

        df = df.copy()
        nof_rows_before_filtering = df.shape[0]        #nof = numer of

        for columns in columns_to_filter:

            for name in columns.names:

                if not name in df.columns.values:       #honestly unnecessary as python would raise an exception itself but yeah
                    raise Exception(f"Key {name} not present in the data frame.")

                if(columns.remove_outliers_iqr):
                    q1=df[name].quantile(q=0.25,interpolation="midpoint")
                    q3=df[name].quantile(q=0.75,interpolation="midpoint")
                    iqr=q3-q1
                    iqr_upper_border=q3+iqr*1.5
                    iqr_lower_border=q1-iqr*1.5
                    df = df.loc[(df[name]<iqr_upper_border) & (df[name] > iqr_lower_border)]

                if(columns.remove_outliers_hard_limit is not None):
                    df = df.loc[df[name]<columns.remove_outliers_hard_limit]
                
                if(columns.filter_is_exactly is not None):
                    df = df.loc[df[name]==columns.filter_is_exactly]
                
                if(columns.filter_is_larger is not None):
                    df = df.loc[df[name]>columns.filter_is_larger]
                
                if(columns.filter_is_larger_or_equal is not None):
                    df = df.loc[df[name]>=columns.filter_is_larger_or_equal]

                if(columns.filter_is_smaller is not None):
                    df = df.loc[df[name]<columns.filter_is_smaller]

                if(columns.filter_is_smaller_or_equal is not None):
                    df = df.loc[df[name]<=columns.filter_is_smaller_or_equal]

                if(columns.filter_top_n is not None):
                    list_top_n = df[name].value_counts().head(columns.filter_top_n).index.tolist()      #100% sure there is a better way to do this
                    df = df.loc[df[name].isin(list_top_n)]                
                            
        nof_rows_after_filtering = df.shape[0]

        if self.verbose:
            print(f"Removed {nof_rows_after_filtering - nof_rows_before_filtering} rows during filtering.")
            print(f"{nof_rows_after_filtering} rows remaining.")
        return df


class ColumnsToFilter:

    def __init__(self, names, remove_outliers_iqr=False, remove_outliers_hard_limit=None, filter_is_exactly=None, filter_top_n=None, filter_is_between=None, filter_is_larger=None, filter_is_larger_or_equal=None, filter_is_smaller=None, filter_is_smaller_or_equal=None):
        self.names = names
        self.remove_outliers_iqr = remove_outliers_iqr
        self.remove_outliers_hard_limit = remove_outliers_hard_limit
        self.filter_is_exactly = filter_is_exactly
        self.filter_top_n = filter_top_n
        self.filter_is_larger = filter_is_larger
        self.filter_is_larger_or_equal = filter_is_larger_or_equal
        self.filter_is_smaller = filter_is_smaller
        self.filter_is_smaller_or_equal = filter_is_smaller_or_equal

def test_iqr():
    df = pd.DataFrame(data=[8,8,9,1,10,9,10,110],columns=["numeric_values"])
    print(df)
    filter1=DataFilter()
    df =filter1.data_filter(df,[ColumnsToFilter(["numeric_values"],remove_outliers_iqr=True)])
    assert df["numeric_values"].values.tolist()==[8, 8, 9, 10, 9, 10]
    print("\n ----\n test_iqr passed!\n ----")

    return

def test_iqr_for_non_existent_column():
    df = pd.DataFrame(data=[8,8,9,1,10,9,10,110],columns=["numeric_values"])
    filter1=DataFilter()

    try:
        df =filter1.data_filter(df,[ColumnsToFilter(["non-existent_column"],remove_outliers_iqr=True)])
    except Exception:
        print("\n ----\n test_iqr_for_non_existent_column passed!\n ----")
    else:
        raise Exception
    return

def test_remove_outliers_hard_limit_10():
    df = pd.DataFrame(data=[8,8,9,1,10,9,10,110],columns=["numeric_values"])
    filter1=DataFilter()

    df = filter1.data_filter(df,[ColumnsToFilter(["numeric_values"],remove_outliers_hard_limit=10)])
    
    assert df["numeric_values"].values.tolist()==[8, 8, 9, 1, 9]
    print("\n ----\n test_remove_outliers_hard_limit_10 passed!\n ----")

    return

def test_remove_outliers_hard_limit_10_multiple_columns():
    df = pd.DataFrame(data=[["Foo",1,47.11],["Bar",3,5],["FooBar",3,15]],columns=["string","int","decimals"])
    print(df)
    filter1=DataFilter()
    df = filter1.data_filter(df,[ColumnsToFilter(["int","decimals"],remove_outliers_hard_limit=10)])
    
    assert df.values.tolist()==[['Bar', 3, 5.0]]
    print("\n ----\n test_remove_outliers_hard_limit_10_multiple_columns passed!\n ----")

    return

def test_filter_is_exactly():
    df = pd.DataFrame(data=[["Foo",1,47.11],["Bar",3,5],["FooBar",3,15]],columns=["string","int","decimals"])
    print(df)
    filter1=DataFilter()
    df = filter1.data_filter(df,[ColumnsToFilter(["int"],filter_is_exactly=3)])
    assert df.values.tolist()==[['Bar', 3, 5.0], ['FooBar', 3, 15.0]]
    print("\n ----\n test_filter_is_exactly passed!\n ----")

def test_filter_top_2():
    df = pd.DataFrame(data=[["Foo",1,47.11],["Foo",3,5],["Bar",3,15],["Bar",3,15],["FooBar",3,15]],columns=["string","int","decimals"])
    print(df)
    filter1=DataFilter()
    df = filter1.data_filter(df,[ColumnsToFilter(["string"],filter_top_n=2)])
    print(df.values.tolist())
    assert df.values.tolist()==[['Foo', 1, 47.11], ['Foo', 3, 5.0], ['Bar', 3, 15.0], ['Bar', 3, 15.0]]
    print("\n ----\n test_filter_top_2 passed!\n ----")

def test_filter_is_between():
    df = pd.DataFrame(data=[["Foo",1,47.11],["Foo",2,5],["Bar",3,15],["Bar",3,15],["FooBar",4,15]],columns=["string","int","decimals"])
    print(df)
    filter1=DataFilter()
    df_incl_incl = filter1.data_filter(df, [ColumnsToFilter(["int"], filter_is_larger_or_equal=2, filter_is_smaller_or_equal=4)])
    df_incl_excl = filter1.data_filter(df,[ColumnsToFilter(["int"],filter_is_larger_or_equal=2, filter_is_smaller=4)])
    df_excl_incl = filter1.data_filter(df,[ColumnsToFilter(["int"],filter_is_larger=2, filter_is_smaller_or_equal=4)])
    df_excl_excl = filter1.data_filter(df,[ColumnsToFilter(["int"],filter_is_larger=2, filter_is_smaller=4)])

    print(df_incl_incl.values.tolist())
    assert df_incl_incl.values.tolist()==[["Foo",2,5], ['Bar', 3, 15.0], ['Bar', 3, 15.0],["FooBar",4,15]]
    assert df_incl_excl.values.tolist()==[["Foo",2,5], ['Bar', 3, 15.0], ['Bar', 3, 15.0]]
    assert df_excl_incl.values.tolist()==[['Bar', 3, 15.0], ['Bar', 3, 15.0], ['FooBar', 4, 15.0]]
    assert df_excl_excl.values.tolist()==[['Bar', 3, 15.0], ['Bar', 3, 15.0]]

    print("\n ----\n test_filter_is_between passed!\n ----")


test_iqr()

test_iqr_for_non_existent_column()

test_remove_outliers_hard_limit_10()

test_remove_outliers_hard_limit_10_multiple_columns()

test_filter_is_exactly()

test_filter_top_2()

test_filter_is_between()