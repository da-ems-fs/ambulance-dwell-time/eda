import pandas as pd

class Importer:

    def __init__(self, column_separator = '\t'):
        self.column_separator = column_separator

    def import_file(self, filename, column_names):
        return pd.read_csv(filename,names = column_names,index_col=False, sep=self.column_separator, engine="python")