import pandas as pd
from src.data.datapreparer import DataPreparer

df_minutes, column_names_minutes_times = DataPreparer().load_standard_for_eda(top10=False)

df_minutes[["Code","Urgent", "HourOfDay", "8 - MinutesAtDestination"]].to_csv("report_anonymous.csv", sep = '\t', index=False)