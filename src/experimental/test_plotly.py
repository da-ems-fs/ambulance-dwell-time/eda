import plotly.express as px
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import seaborn as sns
from datapreparer import DataPreparer

df_minutes, column_names_minutes_times = DataPreparer().load_standard_for_eda(top10=True)

df_minutes_plot = df_minutes[["HourOfDay", "Code", "8 - MinutesAtDestination"]]
df_minutes_plot = df_minutes_plot.sort_values(by="HourOfDay")

print(df_minutes_plot.columns.values)
print(df_minutes_plot.head(10))

list_top_codes = df_minutes["Code"].value_counts().index.tolist()

print(f"lalal {list_top_codes}")

#fig = px.box(df_minutes_plot, y="8 - MinutesAtDestination", x="Code", animation_frame="HourOfDay", animation_group="Code",range_y=[0,100], category_orders={"Code":list_top_codes})
fig = px.violin(df_minutes_plot, y="8 - MinutesAtDestination", x="Code", animation_frame="HourOfDay", animation_group="Code",range_y=[0,100], category_orders={"Code":list_top_codes}, points="all")

fig.show()