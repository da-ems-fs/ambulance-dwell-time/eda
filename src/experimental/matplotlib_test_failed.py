import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import seaborn as sns
from importer import Importer;
from cleaner import Cleaner,ColumnsToClean
from datafilter import DataFilter,ColumnsToFilter
from dataconverter import DataConverter, ColumnsToConvert

filename = 'data/export1.csv'
column_names = ["Code","Urgent","Year","Month","DayOfWeek","HourOfDay","3 - SecondsToMissionSite","4 - SecondsAtMissionSite","7 - SecondsToDestination","8 - SecondsAtDestination","DestinationName"]
column_names_times = ["3 - SecondsToMissionSite","4 - SecondsAtMissionSite","7 - SecondsToDestination","8 - SecondsAtDestination"]

importer1 = Importer(column_separator = '\t')
df = importer1.import_file(filename, column_names)

cleaner1 = Cleaner(verbose=True)
df = cleaner1.clean(df,[ColumnsToClean(names=["Urgent"],to_int=True)])
df = cleaner1.clean(df,[ColumnsToClean(names=column_names,drop_na=True)])

filter1=DataFilter()
df=filter1.data_filter(df,[ColumnsToFilter(["Urgent"],filter_is_exactly=1)])

df=filter1.data_filter(df,[ColumnsToFilter(column_names_times,remove_outliers_hard_limit=60*60*5)])

converter1 = DataConverter()
df_minutes = converter1.data_converter(df, [ColumnsToConvert(column_names_times, convert_from_seconds_to_minutes=True, convert_round_decimals=2)])
column_names_minutes_times = ["3 - MinutesToMissionSite","4 - MinutesAtMissionSite","7 - MinutesToDestination","8 - MinutesAtDestination"]

print(df_minutes.head(5))

fig, ax = plt.subplots(figsize=(10, 5))

sns.boxplot(ax= ax, data=df_minutes[["DestinationName","8 - Minutes at destination"]], x="DestinationName", y="8 - Minutes at destination")
plt.setp(ax.get_xticklabels(), rotation=90)


fig = plt.figure()
i=0
im = plt.imshow(df_minutes, animated=True)

def updatefig(*args):
    global i
    if (i<99):
        i += 1
    else:
        i=0
    im.set_array(arr[i])
    return im,
    
ani = animation.FuncAnimation(fig, updatefig,  blit=True)
plt.show()