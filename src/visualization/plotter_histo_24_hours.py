import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from datapreparer import DataPreparer

plt.style.use('dark_background')

def label_axis():
    plt.xlabel("Mittlere Status 8 Zeit [Minuten]")
    plt.ylabel("Relative Häufigkeit [%]")


df_minutes, column_names_minutes_times = DataPreparer().load_standard_for_eda(top10=False)

df_minutes = df_minutes.loc[df_minutes["8 - MinutesAtDestination"]<120]

x = df_minutes["8 - MinutesAtDestination"].values
plt.hist(x, bins=50, color='c', edgecolor='grey', alpha=0.65, density=True)
plt.ylim(0,0.1)
plt.xlim(0,120)
label_axis()
plt.title(f"Status 8 Zeiten - Übersicht (n={x.size})")
plt.savefig(f"plots/histo_24_hours/complete.png", transparent=True)

for i in range(24):
    plt.close()
    df_minutes_this_hour = df_minutes.loc[df_minutes["HourOfDay"]==i]
    x = df_minutes_this_hour["8 - MinutesAtDestination"].values
    plt.hist(x, bins=50, color='c', edgecolor='grey', alpha=0.65, density=True)
    plt.ylim(0,0.1)
    plt.xlim(0,120)
    label_axis()
    plt.title(f"Status 8 Zeiten - Uhrzeit = {i}:00 bis {i}:59 (n={x.size})")

    plt.savefig(f"plots/histo_24_hours/{i}.png", transparent=True)
    print(f"plt {i} exported")