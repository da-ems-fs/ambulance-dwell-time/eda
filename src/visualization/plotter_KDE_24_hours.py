import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from datapreparer import DataPreparer

plt.style.use('dark_background')


df_minutes, column_names_minutes_times = DataPreparer().load_standard_for_eda(top10=False)

df_minutes = df_minutes.loc[df_minutes["8 - MinutesAtDestination"]<120]


pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
g = sns.FacetGrid(df_minutes, row="HourOfDay", hue="HourOfDay", aspect=15, height=.5, palette=pal)

g.map(sns.kdeplot, "8 - MinutesAtDestination", clip_on=False, shade=True, alpha=1, lw=1.5, bw=.2)
g.map(sns.kdeplot, "8 - MinutesAtDestination", clip_on=False, color="w", lw=2, bw=.2)
g.map(plt.axhline, y=0, lw=2, clip_on=False)

def label(x, color, label):
    ax = plt.gca()
    ax.text(0, .2, label, fontweight="bold", color=color,
            ha="left", va="bottom", transform=ax.transAxes)


g.map(label, "8 - MinutesAtDestination")

g.fig.subplots_adjust(hspace=1)

g.set_titles("")
g.set(yticks=[])
g.despine(bottom=True, left=True)

fig = g.fig

fig.savefig("plots/KDE_24_hours.png")
print("file saved")