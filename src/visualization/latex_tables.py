import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from src.data.cHDI import cHDI

def create_summary(df_dict, val_c, name, latex_table_dir):
    summary = {}
    for df_name, df_v in df_dict.items():
        df_vc = df_v[val_c]
        summary[df_name] = {"count":len(df_vc), "mean":np.mean(df_vc), "median":np.median(df_vc), "p90":np.quantile(df_vc, .9), "p80":np.quantile(df_vc, .8),
        "cHDI90":cHDI(df_vc,90)[2], "cHDI80":cHDI(df_vc,80)[2], "variance":np.var(df_vc), "SD":np.std(df_vc)}
    
    summary_to_tex(summary, name, latex_table_dir)

def summary_to_tex(summary, name, latex_table_dir): 
    df_summary = pd.DataFrame(summary)

    df_summary.round(2).to_latex(f"{latex_table_dir}/{name}.pgf")