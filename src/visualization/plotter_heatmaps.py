import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from src.data.cHDI import cHDI

def plot_heatmaps(datasets, val_col, ops, fig_dir):
    for ds in datasets:
        df = ds["df"]
        df = df.copy()
        for op in ops:
            df_cross = df[[ds["index"], ds["column"], val_col]].pivot_table(index=ds["index"], columns=ds["column"], aggfunc=op["func"])
            df_cross.columns = df_cross.columns.droplevel()
            df_cross = df_cross.round(0)
            df_cross = df_cross.fillna(0)
            plt.figure(figsize=(ds["figw"]-op["figw_minus"],ds["figh"]))
            sns.heatmap(df_cross, annot=True, fmt="g", square=True, cbar=op["cbar"], cmap=op["cmap"], vmin=0, vmax=op["vmax"])
            plt.savefig(f"{fig_dir}/{ds['name']}_{op['name']}.png", bbox_inches="tight", dpi=800)
            plt.clf()

def cHDI_aggfunc(data):
    cHDI_percentage = 80
    return cHDI(data, cHDI_percentage)[2]