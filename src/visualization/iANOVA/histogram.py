import matplotlib.pyplot as plt
import numpy as np


def histo(x, xlabel=False, ylabel=False):
    max_x=120
    plt.hist(x, range=[0,max_x], bins=max_x, density=False)
    if xlabel: plt.xlabel(xlabel)
    if ylabel: plt.ylabel(ylabel)

def example_histos_3(df, groups_column, three_items_of_group, times_column="8 - MinutesAtDestination"):
    fig, axs = plt.subplots(nrows=1, ncols=3, sharey=True, figsize=(15,5))
    max_x=120
    for i, ax in zip(three_items_of_group, axs.flat):        
        x=df.loc[df[groups_column]==i][times_column].values
        ax.hist(x, range=[0,max_x], bins=max_x, density=False)
        print(f"i={i}, Achse={ax}")