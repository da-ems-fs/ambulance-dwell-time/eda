import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from src.data.aggregator import mean_and_variance_of_df_by_group

def mean_and_variance_bar(df, groups_column, times_column, fig_dir):

    matplotlib.use("pgf")
    matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
    })
    plt.clf()
    width=0.5
    offset=0.25

    df_aggd=mean_and_variance_of_df_by_group(df, groups_column, times_column)
    x=np.arange(len(df_aggd.index.tolist()))
    bar_std = plt.bar(x+offset, df_aggd["std"], width=width, label="Standard Deviation")
    bar_mean = plt.bar(x, df_aggd["mean"], width=width, label="Mean")
    plt.gcf().legend(loc='upper center', bbox_to_anchor=(0.5, 1.0), ncol=4)
    plt.xticks(ticks=x+width/2, labels=df_aggd.index, rotation=90)
    plt.savefig(f"{fig_dir}/analysis_report/bar_{groups_column}.pgf", bbox_inches="tight")
    return