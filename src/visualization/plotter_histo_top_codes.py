import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from datapreparer import DataPreparer

plt.style.use('dark_background')

def add_markers():
    markerpoints = {
        "p10":np.percentile(x, 10),
        "median":np.median(x),
        "mean":np.mean(x),
        "p90":np.percentile(x, 90)
        }

    for k, v in markerpoints.items():
        plt.axvline(v, color='r', linestyle='dashed', linewidth=1.5, ymax=0.7)


    min_ylim, max_ylim = plt.ylim()
    for k, v in markerpoints.items():
        plt.text(v, max_ylim*0.75, f"{k}: {round(v,1)}", ha="center", rotation=90)

def label_axis():
    plt.xlabel("Status 8 Zeit [Minuten]")
    plt.ylabel("Relative Häufigkeit [%]")



df_minutes, column_names_minutes_times = DataPreparer().load_standard_for_eda(top10=False)

df_minutes = df_minutes.loc[df_minutes["8 - MinutesAtDestination"]<120]

list_top_n = df_minutes["Code"].value_counts().head(20).index.tolist()
df_minutes = df_minutes.loc[df_minutes["Code"].isin(list_top_n)]  

x = df_minutes["8 - MinutesAtDestination"].values
plt.hist(x, bins=50, color='c', edgecolor='grey', alpha=0.65, density=True)
plt.ylim(0,0.15)
plt.xlim(0,120)

add_markers()
label_axis()

plt.title(f"Status 8 Zeiten - Übersicht (n={x.size})")
plt.savefig(f"plots/histo_code_top_20/complete.png", transparent=True)

for i in list_top_n:
    plt.close()
    df_minutes_this_code = df_minutes.loc[df_minutes["Code"]==i]
    i = i.replace("/", "-")
    i = i.replace(" ", "_")
    x = df_minutes_this_code["8 - MinutesAtDestination"].values
    plt.hist(x, bins=50, color='c', edgecolor='grey', alpha=0.65, density=True)
    plt.ylim(0,0.15)
    plt.xlim(0,120)
    add_markers()
    label_axis()
    plt.title(f"Einsatzstichwort = {i} (n={x.size})")
    plt.savefig(f"plots/histo_code_top_20/{i}.png", transparent=True)
    print(f"plt {i} exported")