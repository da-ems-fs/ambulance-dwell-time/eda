import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from datapreparer import DataPreparer

plt.style.use('dark_background')
sns.axes_style({"axes.facecolor": 'white','axes.labelcolor': 'white',"axes.edgecolor": "white"})


df_minutes, column_names_minutes_times = DataPreparer().load_standard_for_eda(top10=False)


histo_8 = sns.distplot(df_minutes["8 - MinutesAtDestination"])
histo_8.set(xlim=(0,None))
histo_8_limited_2_hours = sns.distplot(df_minutes["8 - MinutesAtDestination"], color=[x/255 for x in(8,94,169)])
histo_8_limited_2_hours.set(xlim=(0,120))
sns.despine()

to_plot = {"histo_8":histo_8, "histo_8_limited_2_hours":histo_8_limited_2_hours}

for k,v in to_plot.items():
    figure = v.get_figure()
    figure.savefig(f"plots/{k}.png", transparent=True)