import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from datapreparer import DataPreparer

plt.style.use('dark_background')

def label_axis():
    plt.xlabel("Uhrzeit")
    plt.ylabel("Mittlere Status 8 Zeit [Minuten]")



df_minutes, column_names_minutes_times = DataPreparer().load_standard_for_eda(top10=False)

print(df_minutes.shape)


df_minutes = df_minutes.loc[df_minutes["8 - MinutesAtDestination"]<120]

print(df_minutes.shape)

df_minutes_median_by_HourOfDay = df_minutes[["HourOfDay","8 - MinutesAtDestination"]].groupby("HourOfDay").median()
df_minutes_median_by_HourOfDay["HourOfDay-1"]=df_minutes_median_by_HourOfDay.index
x = df_minutes_median_by_HourOfDay.values
print(x[:,0])
print(df_minutes_median_by_HourOfDay.head(5))
plt.bar(x[:,1], height=x[:,0], color='c', edgecolor='grey', alpha=0.65)
plt.xticks(range(0,24,2))
#plt.ylim(0,0.15)
#plt.xlim(0,120)

label_axis()
plt.title(f"Status 8 Zeiten im Tagesverlauf (n={df_minutes.shape[0]})")
plt.savefig(f"plots/bar_overviews/median_by_HourOfDay.png", transparent=True)