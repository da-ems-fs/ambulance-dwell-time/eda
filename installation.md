# Requirements to run this Jupyter notebook
> __Note:__ This guide assumes you are using a debian based linux distribution
> On other systems, adjust syntax accordingly

- python 3

```shell
sudo apt install python3
```

- the python package installer pip

```shell
sudo apt install python3-pip
```

- ensure that virtual environments are available

```shell
sudo -H pip3 install virtualenv
```

- create environment, e.g. _.expl_
```shell
cd /PATH/TO/PROJECT
python3 -m venv .expl
```

- activate environment, in vscode via ctrl shift P _select python interpreter_ or:
```shell
source .expl/bin/activate
```

> __Note:__ you can theoretically import all required packages via `.expl/bin/pip3 install -r requirements.txt` (This is not neccessarily recommended)

- install Jupyter Server e.g. via vscode marketplace python extension or:
```shell
pip3 install jupyter
```
> __Note:__ if you prefer, install jupyter outside of the virtual environment if you plan to use it on multiple projects

- install ipython kernel
```shell
pip3 install -U ipykernel
```

- install pandas
```shell
pip3 install pandas
```

- install matplotlib
```shell
pip3 install matplotlib
```

- install seaborn
```shell
pip3 install seaborn
```

- ensure that your Jupyter Server uses the correct virtual environment, e.g. `.expl` 
    - either via the vscode GUI (top right corner)
    - or by starting it in a shell of your environment

```shell
source .expl/bin/activate
jupyter notebook
```


## optional .git config
> __Warning:__ committing Jupyter Notebooks without removing cell output may publish sensitive information!

To avoid publishing confidential data, clear the cell outputs of your jupyter notebook before commiting. 
To automate this process, you can use `nbstripout`, a script that removes cell output from Jupyter Notebooks.

install it via
```shell
pip3 install --upgrade nbstripout
```
you may also wish to add `nbstripout` as a pre-commit hook to your git config

to do this manually, use
```shell
cd /path/to/your/project
which nbstripout
$ /path/to/nbstripout
git config filter.nbstripout.clean '/path/to/nbstripout'
git config filter.nbstripout.smudge cat
git config filter.nbstripout.required true
git config diff.ipynb.textconv '/path/to/nbstripout -t'
```

or automatically
```shell
nbstripout --install --attributes .gitattributes
```
to _exclude_ a notebook from `nbstripout` if you want to publish its content, add the following line to the `.gitattributes` file in the repository
```shell
notebook_to_publish.ipynb filter= diff=ipynb
```

For further information on the configuration options, visit the `nbstripout` project on [github.com/kynan/nbstripout](https://www.github.com/kynan/nbstripout)
